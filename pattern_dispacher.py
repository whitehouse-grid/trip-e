import threading
import time
from patterns import pattern_collection

"""
Purpose:
1.  Check for thread and Kill (if reqiured)
2.  Find the right Pattern
3.  Setup the enviornment (if required)
3.  Start a Thread with the pattern

# Threading Doc:[https://docs.python.org/3/library/threading.html]
"""

class Pattern_Dispacher():
    _THREAD_NAME = 'REAL_SLIM_SHADY'

    _thread = None
    _thread_event = threading.Event()
    
    def __init__(self):
        pass

    @staticmethod
    def _identify_pattern(pattern):
        if pattern == 'SOLID':
            return pattern_collection.Pattern_Solid()
        else:
            pass


    @staticmethod
    def _run_pattern(pattern_obj,color):
        Pattern_Dispacher._thread_event.set()
        Pattern_Dispacher._thread = threading.Thread(target=pattern_obj.run, name=Pattern_Dispacher._THREAD_NAME, args=(color,Pattern_Dispacher._thread_event))
        Pattern_Dispacher._thread.daemon = True                            # Daemonize thread
        Pattern_Dispacher._thread.start()                                  # Start the execution

    @staticmethod
    def _kill_current_pattern():
        if Pattern_Dispacher._thread is not None:
            Pattern_Dispacher._thread_event.clear()
            Pattern_Dispacher._thread.join()
            if Pattern_Dispacher._thread.is_alive():
                Pattern_Dispacher._thread.join()
    
    @staticmethod
    def run(pattern,color):
        Pattern_Dispacher._kill_current_pattern()
        _pattern_obj = Pattern_Dispacher._identify_pattern(pattern)
        Pattern_Dispacher._run_pattern(_pattern_obj,color)
    
