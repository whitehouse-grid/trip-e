Click==7.0
Flask==1.0.2
gitdb2==2.0.5
ItsDangerous==1.0.0
Jinja2==2.10
MarkupSafe==1.0
smmap2==2.0.5
Werkzeug==0.14.1
