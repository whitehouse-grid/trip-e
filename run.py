from flask import Flask, jsonify, request
#from rgbMethods import setVal
from pattern_dispacher import Pattern_Dispacher
app = Flask(__name__)


@app.route('/')
def hello():
    return "Hello World!"

@app.route('/trip-e/color/', methods=['GET','POST'])
def hello1():
    if request.method == 'POST':
        resp = dict()
        content=request.get_json(silent=True)
        try:
            _p = content['pattern']
            _c = content['color']
            Pattern_Dispacher.run(_p,_c)
            resp['status'] = 200
        except KeyError:
            resp['status'] = 500
        return jsonify(resp)

if __name__ == '__main__':
    app.run(host='0.0.0.0',port = 5555)
