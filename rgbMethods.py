import RPi.GPIO as IO
import time
red_port = 13
green_port = 26
blue_port = 19
IO.setwarnings(False)
IO.setmode (IO.BCM)
IO.setup(red_port,IO.OUT)
r = IO.PWM(red_port,100)
IO.setup(green_port,IO.OUT)
g = IO.PWM(green_port,100)
IO.setup(blue_port,IO.OUT)
b = IO.PWM(blue_port,100)
r.start(0)
g.start(0)
b.start(0)

def setVal(color, brightness):
    if color == 'r':
        r.ChangeDutyCycle(100-brightness)
    elif color == 'g':
        g.ChangeDutyCycle(100-brightness)
    else:
        b.ChangeDutyCycle(100-brightness)
