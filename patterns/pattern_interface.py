from abc import ABC, abstractmethod

class pattern_interface(ABC):

    @abstractmethod
    def start(self):
        pass

    @abstractmethod
    def run_pattern(self,color,active_flag):
        pass

    @abstractmethod
    def run(self,color,active_flag):
        pass
