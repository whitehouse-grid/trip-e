from .pattern_base import Pattern

class Pattern_Solid(Pattern):
    _color_old = None
    is_set = False
    
    def pattern_run(self,color):
        if not Pattern_Solid._color_old == color:
            Pattern_Solid.is_set = False
        else:
            Pattern_Solid.is_set = True

        if not Pattern_Solid.is_set :
            Pattern._red.ChangeDutyCycle(100 - int(color['red']['brightness']))
            Pattern._green.ChangeDutyCycle(100 - int(color['green']['brightness']))
            Pattern._blue.ChangeDutyCycle(100 - int(color['blue']['brightness']))
            Pattern_Solid.is_set = True
            Pattern_Solid._color_old == color
