import RPi.GPIO as IO


class Pattern(object):
    
    _red = None
    _green = None
    _blue = None
    
    @staticmethod
    def setup():
        if Pattern._red is None or Pattern._blue is None or Pattern._green is None :
            # Physical Pin Numbers
            red_port = 19
            green_port = 13
            blue_port = 26
            IO.setwarnings(False)
            IO.setmode (IO.BCM)
            # Set Pins in Output Mode
            IO.setup(red_port,IO.OUT)
            IO.setup(green_port,IO.OUT)
            IO.setup(blue_port,IO.OUT)

            Pattern._red = IO.PWM(red_port,200)
            Pattern._green = IO.PWM(green_port,200)
            Pattern._blue = IO.PWM(blue_port,200)
            
            Pattern._red.start(0)
            Pattern._blue.start(0)
            Pattern._green.start(0)
        
    def pattern_run(self,color):
        pass
    
    def run(self,color,active_flag):
        Pattern.setup()
        if Pattern._red is not None and Pattern._blue is not None and Pattern._green is not None :
            while active_flag.is_set():
                self.pattern_run(color)

    
